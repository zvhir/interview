<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ $title }}
            </h2>

            <div>
                <x-button onclick="document.getElementById('form').submit()">Save</x-button>
                @if ($context == 'edit')
                <x-button onclick="document.getElementById('form-destroy').submit()" class="bg-red-700 hover:bg-red-500">Delete</x-button>
                @endif
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <form id="form" action="{{$action}}" method="POST" class="mt-6 sm:mt-5 space-y-6 sm:space-y-5">
                        @csrf

                        @if ($context == 'edit')
                            @method('PUT')
                        @endif
                        <div
                            class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                            <label for="slug" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                                Slug
                            </label>
                            <div class="mt-1 sm:mt-0 sm:col-span-2">
                                <div class="max-w-lg flex rounded-md shadow-sm">
                                    <span
                                        class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">
                                        https://mysite.com/blogs/
                                    </span>
                                    <input type="text" name="slug" id="slug" autocomplete="slug" value="{{ $model->slug }}"
                                        class="flex-1 block w-full focus:ring-indigo-500 focus:border-indigo-500 min-w-0 rounded-none rounded-r-md sm:text-sm border-gray-300">
                                </div>
                            </div>
                        </div>

                        <div
                            class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                            <label for="title" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                                Title
                            </label>
                            <div class="mt-1 sm:mt-0 sm:col-span-2">
                                <input value="{{ $model->title }}" id="title" name="title" type="text" autocomplete="title" class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                            </div>
                        </div>

                        <div
                            class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                            <label for="content" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                                Content
                            </label>
                            <div class="mt-1 sm:mt-0 sm:col-span-2">
                                <textarea id="content" name="content" rows="3"
                                    class="max-w-lg shadow-sm block w-full focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">{{ $model->content }}</textarea>
                            </div>
                        </div>

                    </form>

                    @if ($context == 'edit')
                    <form id="form-destroy" action="{{$destroyAction}}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>