Your writing must cover below questions:

# 1. What happen when running `php artisan make:model Post --all`

"--all" artisan option will save the time. It will auto generate Model, Factory and migration files.

1. Model: (app/models/Post.php)
2. Factory: (database/factories/PostFactory.php)
3. Migration: (database/migrations/20210512.......create_posts_table.php)

# 2. What is middleware and how middleware lifecycle works?

Middleware is something like human hand that will filter/control what can be returned / displayed / given access / redirect /  to different kind of situation or user roles. 

There is three type of middleware:

1. Global middleware
2. Route middleware
3. Group middleware

# 3. Give 4 use case of using middleware with implementations (Codes).

1. Stricted page (Admin panel) - If the role is admin, then re-route to admin panel. If the role is user, then re-route to user dashboard

in Kernel.php
```php
protected $routeMiddleware = [
 'admincheck' =>  \App\Http\Middleware\AdminCheck::class,
 ];
```

in app/Http/Middleware/Admin.php.php
```php
   public function handle(Request $request, Closure $next)
    {
        // role == 1 is admin // 2 is user
        if(auth()->user()->role == 1){ 
            return $next($request);
        }
        return redirect('welcome')->with('error','You are not admin.');
    }
```

in route (web.php)
```php
   public function handle(Request $request, Closure $next)
    {
        // role == 1 is admin // 2 is user
        if(auth()->user()->role == 1){ 
            return $next($request);
        }
        return redirect('welcome')->with('error','You are not admin.');
    }
```

2. API - if API authentication token is valid, then retun data, else return forbidden access

3. Page language - If the choosen language is “en” then the displayed route can be shown as “create/user/ahmad” for example. If the choosen language is malay, it can be “cipta/pengguna/ahmad”. This is not necessary but it can be implement as that.

4. Stricted page (Age control) - If the user age is under 18, prevent the user from accessing the site. If the user is 18 above, then display "Find partner page"

in Kernel.php
```php
protected $routeMiddleware = [
 'agecheck' =>  \App\Http\Middleware\AgeCheck::class,
 ];
```
in (web.php)
```php
Route::get('/findpartner', [ HomeController::class, "findpartner" ])->middleware('agecheck');
```
in (Kernel.php)
```php
  'agecheck' =>  \App\Http\Middleware\AgeCheck::class,
```
in (app/Middleware/AgeCheck.php)
```php
   public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->age > 18){
            return $next($request);
        }
        return redirect('dashboard')->withErrors(['You are under 18']);
    }
```

# 4. Explain what `BlockBlacklistedEmail` middleware do.

If a user has do harmful things / scam / abuse / make harrasment.his/her email will be listed to black list. In this case it is listed in config/app.php
 
 ```php
 'blacklisted_emails' => [
        'asd@gmail.com'
    ],
```

# 5. How event system works in laravel.

When we post someting online it is consider an event. When we send message it is consider an event. Event is like we are shouting in a room

Then..

The people there must be a people that listen and make a response. It is called a listener. When event occur listener will do something as response

In this case, when we post, our follower will get notification. When we send message, the receiver will get alert message. The trigger of notification and message is done by listener

# 6. How queue system works in laravel.

Ali message Siti at 8:00 pm. Siti will get a notification / email.. But service like mailchimp usually takes 1 to 5 minutes to send an email. We need to use queue job. The queue will be queued as sequence log in database. It will be processed in the background while Ali/Siti continue browsing the BaitulJannah. This is to ensure they get the best user experiences.

# 7. What is oop and how laravel use oop.

Oop is the style to code an object. It is like we are playing with some object that has attribute / form / type which we can modify and reuse the object. There were grouped into class.

In laravel, oop concept can be seen clearly in Controllers. We are calling models / classes.
# 8. What is ioc container and what is binding.

Sample route binding

in (web.php)

 ```php
Route::get('/{key:username}', [ ProfileController::class, "displayprofile" ]);

```

in (ProfileController.php)
 ```php
   public function displayProfile(Profile $key)
    {
        $data = $key;
        $infos = Profile::where('username', $key->id)->get();
        return view('portfolio')->with(compact( 'infos'));
    }
```

# 9. Take a look in `PostController`, what can be improve/fix/discard/bad? 

We can improve by using Resource controller, so that we can deliver/return only needed data to the blade. Querying can be improved by using ::where. If one user can have many post. Then we surely need to add "belongsto" and "hasmany" in the User model and Post model. It will be more ease.

# 10. Explain laravel service provider and try to create your own provider and implement all previously mentioned laravel feature. 

Provider is basically like a config file, but it is written in the manner of oop-like structure. 

# 11. Briefly explain laravel lifecycle.

Laravel use MVC + R. Everything start with path. Route will decide what to do with the path. Is it just return a view or need a controller so that data can be played / modified / returned in there. If controller is needed, then controller will be called. In controller, we can use Model which it is modelled based on the data with stored in MySQL. The attribute, the table name is all stored in model.

Bonus point if candidate includes diagram for explanation.