<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/findpartner', [ HomeController::class, "findpartner" ])->middleware('agecheck');
Route::get('/admin', [ HomeController::class, "admindashboard" ])->middleware('admincheck');


require __DIR__.'/auth.php';

Route::middleware(['auth', 'block.emails'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::resource('posts', PostController::class);
});