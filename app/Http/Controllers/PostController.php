<?php

namespace App\Http\Controllers;

use App\Events\PostCreated;
use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modelQuery = Post::query();

        if ($q = $request->q) {
            $modelQuery->where('title', 'LIKE', "%{$q}%");
        }

        return view('posts.index', [
            'data' => $modelQuery->paginate(config('app.paginate'))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.template', [
            'context' => 'create',
            'title' => 'Create a post',
            'model' => new Post,
            'action' => route('posts.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'slug' => 'nullable|unique:posts,slug',
            'title' => 'required|string|max:25',
            'content' => 'nullable'
        ]);

        if (! $request->has('slug')) {
            $data['slug'] = Str::slug($request->title);
        }

        $data['user_id'] = $request->user()->id;

        $post = Post::create($data);

        PostCreated::dispatch($post);

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.template', [
            'context' => 'show',
            'model' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.template', [
            'context' => 'edit',
            'title' => 'Edit a post',
            'model' => $post,
            'action' => route('posts.update', $post),
            'destroyAction' => route('posts.destroy', $post),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $data = $request->validate([
            'slug' => 'nullable|unique:posts,slug,' . $post->id,
            'title' => 'required|string|max:25',
            'content' => 'nullable'
        ]);

        if (! $request->has('slug')) {
            $data['slug'] = Str::slug($request->title);
        }

        $data['user_id'] = $request->user()->id;

        $post->update($data);

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index');
    }
}
