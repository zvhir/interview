<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function admindashboard( )
    {
        return view('admindashboard');
    }
    
    public function findpartner( )
    {
        return view('findpartner');
    }
}
