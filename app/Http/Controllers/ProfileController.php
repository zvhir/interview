<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function displayProfile(Profile $key)
    {
        $data = $key;
        // return $data;

        $skills = Skill::where('user_id', $key->id)->get();
        $educations = Education::where('userid', $key->id)->get();
        $experiences = Experience::where('userid', $key->id)->get();
        $references = Reference::where('userid', $key->id)->get();

        // return $data;
        return view('portfolio')->with(compact('data', 'skills', 'educations', 'experiences', 'references'));
    }
}
