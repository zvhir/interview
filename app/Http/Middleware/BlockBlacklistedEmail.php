<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\BlacklistedEmailAccessAttempt;

class BlockBlacklistedEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (
            Auth::check() && 
            in_array($email = $request->user()->email, config('app.blacklisted_emails'))
        ) {
            // Notification::send(User::first(), new BlacklistedEmailAccessAttempt($email));

            return redirect('/');
        }
        
        return $next($request);
    }
}
