<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
         // role == 1 is admin // 2 is user
         if(auth()->user()->role == 1){ 
            return $next($request);
        }
        return redirect('dashboard')->withErrors(['You are not admin']);
    }
}
